import 'package:crybaby/di/get_it.dart';
import 'package:crybaby/l10n/app_localizations.dart';
import 'package:crybaby/state/app_state.dart';
import 'package:crybaby/state/language_preference.dart';
import 'package:crybaby/state/theme_preference.dart';
import 'package:crybaby/theme/theme.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:go_router/go_router.dart';
import 'package:redux/redux.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GoRouter _router = getIt<GoRouter>();
  final Store<AppState> _store = getIt<Store<AppState>>();

  String _onGenerateTitle(final BuildContext context) {
    return AppLocalizations.of(context).appName;
  }

  Widget _dynamicColorBuilder(
    final ColorScheme? lightColorScheme,
    final ColorScheme? darkColorScheme,
  ) {
    return StoreConnector<AppState, AppState>(
      builder: (final BuildContext context, final AppState state) {
        final ThemeData lightTheme = lightThemeFromColorScheme(
          colorScheme: lightColorScheme,
        );
        final ThemeData darkTheme = darkThemeFromColorScheme(
          colorScheme: darkColorScheme,
        );
        return MaterialApp.router(
          key: const Key('Crybaby'),
          routerConfig: _router,
          onGenerateTitle: _onGenerateTitle,
          theme: lightTheme,
          darkTheme: darkTheme,
          themeMode: state.themePreference.toThemeMode(),
          locale: state.languagePreference.toLocale(),
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
        );
      },
      converter: (final Store<AppState> store) => store.state,
    );
  }

  @override
  Widget build(final BuildContext context) {
    return StoreProvider<AppState>(
      store: _store,
      child: DynamicColorBuilder(
        builder: _dynamicColorBuilder,
      ),
    );
  }
}
