import 'package:hive/hive.dart';

part 'reason.g.dart';

@HiveType(typeId: Reason.typeId)
class Reason extends HiveObject {
  Reason(
    this.title,
  );

  static const int typeId = 1;

  @HiveField(0)
  final String title;
}
