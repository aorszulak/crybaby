import 'package:crybaby/data/cry.dart';
import 'package:hive/hive.dart';

// ignore: avoid_classes_with_only_static_members
class CryService {
  static final Box<Cry> box = Hive.box<Cry>('cries');
  static Future<void> add(final Cry cry) async {
    await box.add(cry);
  }
}
