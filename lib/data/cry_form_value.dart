import 'package:crybaby/data/reason.dart';
import 'package:hive/hive.dart';

class CryFormValue {
  CryFormValue({
    required this.timestamp,
    required this.duration,
    required this.intensity,
    required this.reasons,
  });

  DateTime timestamp;
  Duration duration;
  int intensity;
  HiveList<Reason> reasons;
}
