import 'package:crybaby/data/cry.dart';
import 'package:crybaby/data/reason.dart';
import 'package:hive/hive.dart';

Future<void> initHive() async {
  Hive
    ..registerAdapter(CryAdapter())
    ..registerAdapter(ReasonAdapter());
  await Hive.openBox<Cry>('cries');
  await Hive.openBox<Reason>('reasons');
}
