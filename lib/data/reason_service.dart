import 'package:crybaby/data/reason.dart';
import 'package:hive/hive.dart';

// ignore: avoid_classes_with_only_static_members
class ReasonService {
  static final Box<Reason> box = Hive.box<Reason>('reasons');
  static Future<void> add(final Reason reason) async {
    await box.add(reason);
  }
}
