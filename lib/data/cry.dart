import 'package:crybaby/data/reason.dart';
import 'package:hive/hive.dart';

part 'cry.g.dart';

@HiveType(typeId: Cry.typeId)
class Cry extends HiveObject {
  Cry(
    this.duration,
    this.intensity,
    this.timestamp,
    this.reasons,
  );

  static const int typeId = 0;

  @HiveField(0)
  DateTime timestamp;
  @HiveField(1)
  int duration;
  @HiveField(2)
  int intensity;
  @HiveField(3)
  HiveList<Reason> reasons;
}
