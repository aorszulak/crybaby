import 'package:dynamic_color/dynamic_color.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

const Color _brandColor = Colors.lightBlue;

final ColorScheme brandLightColorScheme = ColorScheme.fromSeed(
  seedColor: _brandColor,
);
final ColorScheme brandDarkColorScheme = ColorScheme.fromSeed(
  seedColor: _brandColor,
  brightness: Brightness.dark,
);

ThemeData lightThemeFromColorScheme({
  final ColorScheme? colorScheme,
}) {
  ColorScheme? lightColorScheme;
  if (colorScheme == null) {
    lightColorScheme = brandLightColorScheme;
  } else {
    lightColorScheme = colorScheme.harmonized();
  }
  final ThemeData theme = FlexThemeData.light(
    colorScheme: lightColorScheme,
    surfaceMode: FlexSurfaceMode.level,
    blendLevel: 9,
    subThemesData: const FlexSubThemesData(
      defaultRadius: 32,
      cardElevation: 1,
    ),
    keyColors: const FlexKeyColors(),
    visualDensity: FlexColorScheme.comfortablePlatformDensity,
    useMaterial3: true,
  );
  return theme.copyWith(
    listTileTheme: theme.listTileTheme.copyWith(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(32)),
      ),
    ),
    dropdownMenuTheme: DropdownMenuThemeData(
      menuStyle: MenuStyle(
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
        ),
      ),
    ),
    popupMenuTheme: PopupMenuThemeData(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
    ),
    dialogTheme: DialogTheme(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
    ),
    appBarTheme: theme.appBarTheme.copyWith(
      systemOverlayStyle: const SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent,
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
      ),
    ),
  );
}

ThemeData darkThemeFromColorScheme({
  final ColorScheme? colorScheme,
}) {
  ColorScheme? darkColorScheme;

  if (colorScheme == null) {
    darkColorScheme = brandDarkColorScheme;
  } else {
    darkColorScheme = colorScheme.harmonized();
  }

  final ThemeData theme = FlexThemeData.dark(
    colorScheme: darkColorScheme,
    surfaceMode: FlexSurfaceMode.level,
    blendLevel: 9,
    subThemesData: const FlexSubThemesData(
      defaultRadius: 32,
      cardElevation: 1,
    ),
    keyColors: const FlexKeyColors(),
    visualDensity: FlexColorScheme.comfortablePlatformDensity,
    useMaterial3: true,
  );
  return theme.copyWith(
    listTileTheme: theme.listTileTheme.copyWith(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(32)),
      ),
    ),
    appBarTheme: theme.appBarTheme.copyWith(
      systemOverlayStyle: const SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent,
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
        systemStatusBarContrastEnforced: false,
      ),
    ),
  );
}
