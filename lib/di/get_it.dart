import 'package:crybaby/router/app_router.dart';
import 'package:crybaby/state/app_state.dart';
import 'package:crybaby/state/store.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:redux/redux.dart';

final GetIt getIt = GetIt.instance;

Future<void> setup() async {
  getIt
    ..registerSingletonAsync<Store<AppState>>(createStore)
    ..registerSingletonAsync<GoRouter>(createAppRouter);
  await getIt.allReady();
}
