import 'dart:async';

import 'package:crybaby/pages/cry_add.dart';
import 'package:crybaby/pages/cry_index.dart';
import 'package:crybaby/pages/dashboard.dart';
import 'package:crybaby/pages/home.dart';
import 'package:crybaby/pages/reason_add.dart';
import 'package:crybaby/pages/reason_index.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

part 'app_router.g.dart';

Future<GoRouter> createAppRouter() async {
  final GoRouter appRouter = GoRouter(
    routes: $appRoutes,
    initialLocation: '/dashboard',
    debugLogDiagnostics: kDebugMode,
  );
  return appRouter;
}

final GlobalKey<NavigatorState> _sectionANavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'sectionANav');
final GlobalKey<NavigatorState> _sectionBNavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'sectionBNav');
final GlobalKey<NavigatorState> _sectionCNavigatorKey =
    GlobalKey<NavigatorState>(debugLabel: 'sectionCNav');

@TypedStatefulShellRoute<MyShellRouteData>(
  branches: <TypedStatefulShellBranch<StatefulShellBranchData>>[
    TypedStatefulShellBranch<BranchAData>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<DashboardRoute>(path: '/dashboard'),
      ],
    ),
    TypedStatefulShellBranch<BranchBData>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<CryIndexRoute>(
          path: '/cry',
          routes: <TypedRoute<RouteData>>[
            TypedGoRoute<CryAddRoute>(
              path: 'add',
            ),
          ],
        ),
      ],
    ),
    TypedStatefulShellBranch<BranchCData>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<ReasonIndexRoute>(
          path: '/reason',
          routes: <TypedRoute<RouteData>>[
            TypedGoRoute<ReasonAddRoute>(
              path: 'add',
            ),
          ],
        ),
      ],
    ),
  ],
)
class MyShellRouteData extends StatefulShellRouteData {
  const MyShellRouteData();

  @override
  Widget builder(
    final BuildContext context,
    final GoRouterState state,
    final StatefulNavigationShell navigationShell,
  ) {
    return navigationShell;
  }

  static const String $restorationScopeId = 'restorationScopeId';

  static Widget $navigatorContainerBuilder(
      final BuildContext context,
      final StatefulNavigationShell navigationShell,
      final List<Widget> children) {
    return HomePage(
      navigationShell: navigationShell,
      children: children,
    );
  }
}

class BranchAData extends StatefulShellBranchData {
  const BranchAData();

  static final GlobalKey<NavigatorState> $navigatorKey = _sectionANavigatorKey;
  static const String $restorationScopeId = 'restorationScopeId';
}

class BranchBData extends StatefulShellBranchData {
  const BranchBData();

  static final GlobalKey<NavigatorState> $navigatorKey = _sectionBNavigatorKey;
  static const String $restorationScopeId = 'restorationScopeId';
}

class BranchCData extends StatefulShellBranchData {
  const BranchCData();

  static final GlobalKey<NavigatorState> $navigatorKey = _sectionCNavigatorKey;
  static const String $restorationScopeId = 'restorationScopeId';
}

class CryIndexRoute extends GoRouteData {
  const CryIndexRoute();

  @override
  Widget build(final BuildContext context, final GoRouterState state) {
    return const CryIndexPage();
  }
}

class ReasonIndexRoute extends GoRouteData {
  const ReasonIndexRoute();

  @override
  Widget build(final BuildContext context, final GoRouterState state) {
    return const ReasonIndexPage();
  }
}

class ReasonAddRoute extends GoRouteData {
  const ReasonAddRoute();

  @override
  Widget build(final BuildContext context, final GoRouterState state) {
    return const ReasonAddPage();
  }
}

class CryAddRoute extends GoRouteData {
  const CryAddRoute();

  @override
  Widget build(final BuildContext context, final GoRouterState state) {
    return const CryAddPage();
  }
}

class DashboardRoute extends GoRouteData {
  const DashboardRoute();

  @override
  Widget build(final BuildContext context, final GoRouterState state) {
    return const DashboardPage();
  }
}
