import 'dart:math' as math;

import 'package:collection/collection.dart';
import 'package:crybaby/data/cry.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';

class CriesLineChartCard extends StatefulWidget {
  const CriesLineChartCard({
    required this.box,
    super.key,
  });

  final Box<Cry> box;

  @override
  State<CriesLineChartCard> createState() => _CriesLineChartCardState();
}

class _CriesLineChartCardState extends State<CriesLineChartCard> {
  Duration _duration = const Duration(days: 7);

  @override
  Widget build(final BuildContext context) {
    const FlGridData gridData = FlGridData(
      show: false,
    );
    final DateTime now = DateTime.now();
    final Map<int, int> daysWithCries = <int, int>{};
    final Iterable<Cry> cries = widget.box.values
        .where(
          (final Cry element) => element.timestamp.isAfter(
            now.subtract(
              _duration,
            ),
          ),
        )
        .sorted(
            (final Cry a, final Cry b) => a.timestamp.compareTo(b.timestamp));
    if (cries.isEmpty) {
      return const Card(
        child: Center(
          child: Text(
            'Add cries to see the chart',
          ),
        ),
      );
    }
    for (final Cry cry in cries) {
      final int diff = cry.timestamp.difference(now).inDays;
      final int previous = daysWithCries[diff] ?? 0;
      final int current = previous + 1;
      daysWithCries[diff] = current;
    }
    final List<FlSpot> spots = daysWithCries
        .map((final int key, final int value) => MapEntry<int, FlSpot>(
            key, FlSpot(key.toDouble(), value.toDouble())))
        .values
        .toList();

    Widget bottomTitleWidgets(final double value, final TitleMeta meta) {
      if (value.toInt() == -(_duration.inDays)) {
      } else if (value.toInt() == 0) {
      } else {
        return Container();
      }
      final DateFormat dateFormater = DateFormat('MMM dd');

      return SideTitleWidget(
        axisSide: meta.axisSide,
        space: 16,
        child: Text(
            dateFormater.format(
              DateTime.now().add(
                Duration(
                  days: value.toInt(),
                ),
              ),
            ),
            textAlign: TextAlign.center),
      );
    }

    final SideTitles bottomTitles = SideTitles(
      showTitles: true,
      reservedSize: 48,
      interval: 1,
      getTitlesWidget: bottomTitleWidgets,
    );

    Widget leftTitleWidgets(final double value, final TitleMeta meta) {
      if (value.toInt().toDouble() != value) {
        return Container();
      }

      return Text(value.toInt().toString(), textAlign: TextAlign.center);
    }

    SideTitles leftTitles() => SideTitles(
          getTitlesWidget: leftTitleWidgets,
          showTitles: true,
          interval: 1,
          reservedSize: 40,
        );

    final LineChartBarData lineChartBarData1_1 = LineChartBarData(
      isCurved: true,
      barWidth: 4,
      isStrokeCapRound: true,
      spots: spots,
    );

    final FlTitlesData titlesData1 = FlTitlesData(
      bottomTitles: AxisTitles(
        sideTitles: bottomTitles,
      ),
      rightTitles: const AxisTitles(),
      topTitles: const AxisTitles(),
      leftTitles: AxisTitles(
        sideTitles: leftTitles(),
      ),
    );

    final List<LineChartBarData> lineBarsData1 = <LineChartBarData>[
      lineChartBarData1_1,
    ];
    final FlBorderData borderData = FlBorderData(
      show: false,
    );
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const Text('Cries'),
              DropdownButton<Duration>(
                borderRadius: BorderRadius.circular(16),
                padding: const EdgeInsets.all(8),
                value: _duration,
                items: const <DropdownMenuItem<Duration>>[
                  DropdownMenuItem<Duration>(
                    value: Duration(days: 7),
                    child: Text('last week'),
                  ),
                  DropdownMenuItem<Duration>(
                    value: Duration(days: 31),
                    child: Text('last month'),
                  ),
                ],
                onChanged: (final Duration? value) {
                  if (value == null) return;
                  setState(() {
                    _duration = value;
                  });
                },
              )
            ],
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(
              top: 16,
              left: 8,
              right: 8,
              bottom: 82,
            ),
            child: LineChart(
              LineChartData(
                gridData: gridData,
                titlesData: titlesData1,
                borderData: borderData,
                lineBarsData: lineBarsData1,
                minX: -(_duration.inDays.toDouble()),
                maxX: 0,
                maxY: math.max(
                    spots.map((final FlSpot e) => e.y).toList().max, 2),
                minY: 0,
                baselineY: 0,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
