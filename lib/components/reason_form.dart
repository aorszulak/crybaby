import 'package:crybaby/data/reason.dart';
import 'package:crybaby/data/reason_form_value.dart';
import 'package:crybaby/l10n/app_localizations.dart';
import 'package:flutter/material.dart';

class ReasonForm extends StatefulWidget {
  const ReasonForm({
    required this.formKey,
    this.initialValues,
    super.key,
  });

  final GlobalKey<FormState> formKey;
  final Reason? initialValues;

  @override
  ReasonFormState createState() => ReasonFormState();
}

Duration crossFadeDuration = const Duration(milliseconds: 250);

class ReasonFormState extends State<ReasonForm> {
  final ReasonFormValue reason = ReasonFormValue();

  String? isEmpty(final String? value) {
    if (value != null && value.isNotEmpty) {
      return null;
    } else {
      return AppLocalizations.of(context).componentEntryFormNotEmpty;
    }
  }

  String? isInt(final String? value) {
    if (value != null && int.tryParse(value) != null) {
      return null;
    } else {
      return AppLocalizations.of(context).componentEntryFormInteger;
    }
  }

  @override
  Widget build(final BuildContext context) {
    return Form(
      key: widget.formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Column(
          children: <Widget>[
            TextFormField(
              initialValue: reason.title,
              onChanged: (final String value) {
                setState(() {
                  reason.title = value;
                });
              },
              validator: isEmpty,
            ),
          ],
        ),
      ),
    );
  }
}
