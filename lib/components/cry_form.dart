import 'package:crybaby/data/cry_form_value.dart';
import 'package:crybaby/data/reason.dart';
import 'package:crybaby/data/reason_service.dart';
import 'package:crybaby/l10n/app_localizations.dart';
import 'package:duration_picker/duration_picker.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';

class CryForm extends StatefulWidget {
  const CryForm({
    required this.formKey,
    this.initialValues,
    super.key,
  });

  final GlobalKey<FormState> formKey;
  final Reason? initialValues;

  @override
  CryFormState createState() => CryFormState();
}

Duration crossFadeDuration = const Duration(milliseconds: 250);

class CryFormState extends State<CryForm> {
  final CryFormValue cry = CryFormValue(
    timestamp: DateTime.now(),
    duration: const Duration(minutes: 5),
    intensity: 1,
    reasons: HiveList<Reason>(ReasonService.box),
  );

  String? isEmpty(final String? value) {
    if (value != null && value.isNotEmpty) {
      return null;
    } else {
      return AppLocalizations.of(context).componentEntryFormNotEmpty;
    }
  }

  String? isInt(final String? value) {
    if (value != null && int.tryParse(value) != null) {
      return null;
    } else {
      return AppLocalizations.of(context).componentEntryFormInteger;
    }
  }

  @override
  Widget build(final BuildContext context) {
    final DateFormat dateFormater = DateFormat('y-MM-dd', 'pl');
    final DateFormat timeFormater = DateFormat('HH:mm', 'pl');

    return Form(
      key: widget.formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Column(
          children: <Widget>[
            ListTile(
              title: const Text('Date'),
              onTap: () async {
                final DateTime now = DateTime.now();
                final DateTime? datetime = await showDatePicker(
                  context: context,
                  firstDate: now.subtract(const Duration(days: 7)),
                  lastDate: DateTime(
                    now.year + 1,
                  ),
                );
                if (datetime == null) return;
                if (!mounted) return;
                setState(() {
                  cry.timestamp = cry.timestamp.copyWith(
                    year: datetime.year,
                    month: datetime.month,
                    day: datetime.day,
                  );
                });
              },
              subtitle: Text(dateFormater.format(cry.timestamp)),
            ),
            ListTile(
              title: const Text('Time'),
              onTap: () async {
                final TimeOfDay? timeOfDate = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.now(),
                  initialEntryMode: TimePickerEntryMode.dialOnly,
                );
                if (timeOfDate == null) return;
                if (!mounted) return;
                setState(() {
                  cry.timestamp = cry.timestamp.copyWith(
                    hour: timeOfDate.hour,
                    minute: timeOfDate.minute,
                  );
                });
              },
              subtitle: Text(timeFormater.format(cry.timestamp)),
            ),
            ListTile(
              title: const Text('Duration'),
              subtitle: Text('${cry.duration.inMinutes} minutes'),
              onTap: () async {
                final Duration? duration = await showDurationPicker(
                  context: context,
                  initialTime: cry.duration,
                );
                if (duration == null) return;
                if (!mounted) return;
                setState(() {
                  cry.duration = duration;
                });
              },
            ),
            ListTile(
              title: const Text('Intensity'),
              subtitle: Slider(
                max: 10,
                divisions: 10,
                onChanged: (final double value) {
                  setState(() {
                    cry.intensity = value.toInt();
                  });
                },
                value: (cry.intensity.toDouble()),
              ),
            ),
            ListTile(
              title: const Text('Reasons'),
              // subtitle: Row(children: [],)
              // subtitle: Row()
              subtitle: ValueListenableBuilder<Box<Reason>>(
                valueListenable: ReasonService.box.listenable(),
                builder: (final BuildContext context, final Box<Reason> box,
                    final Widget? widget) {
                  return Row(
                    children: box.values
                        .map((final Reason reason) => Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 4),
                              child: InputChip(
                                label: Text(reason.title),
                                selected: cry.reasons.contains(reason),
                                onSelected: (final bool value) {
                                  setState(() {
                                    if (value) {
                                      cry.reasons.add(reason);
                                    } else {
                                      cry.reasons.remove(reason);
                                    }
                                  });
                                },
                              ),
                            ))
                        .toList(),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
