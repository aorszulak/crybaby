import 'dart:async';

import 'package:crybaby/data/init.dart';
import 'package:crybaby/di/get_it.dart' as get_it;
import 'package:crybaby/my_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  unawaited(SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.edgeToEdge,
  ));

  unawaited(SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]));
  await Hive.initFlutter();
  await initHive();
  await get_it.setup();

  runApp(const MyApp());
}
