import 'dart:developer';

import 'package:crybaby/state/app_state_actions.dart';
import 'package:crybaby/state/language_preference.dart';
import 'package:crybaby/state/theme_preference.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_state.g.dart';

@JsonSerializable()
class AppState {
  AppState({
    this.themePreference = ThemePreference.light,
    this.languagePreference = LanguagePreference.system,
    // this.reasons = const <Reason>[],
    // this.cries = const <Cry>[],
  });

  factory AppState.fromJson(final Map<String, dynamic> json) {
    if (kDebugMode) {
      log('AppState.fromJson: ${json.toString()}');
    }
    return _$AppStateFromJson(json);
  }

  final LanguagePreference languagePreference;
  // final List<Reason> reasons;
  // final List<Cry> cries;
  final ThemePreference themePreference;

  static AppState? tryFromJson(final dynamic json) {
    try {
      return AppState.fromJson(json);
    } catch (e) {
      return null;
    }
  }

  Map<String, dynamic> toJson() {
    if (kDebugMode) {
      log('AppState.toJson: ${_$AppStateToJson(this).toString()}');
    }
    return _$AppStateToJson(this);
  }

  @override
  String toString() {
    return toJson().toString();
  }

  AppState copyWith(final UpdateStateAction updatedValues) => AppState(
        themePreference: updatedValues.themePreference ?? themePreference,
        languagePreference:
            updatedValues.languagePreference ?? languagePreference,
        // reasons: updatedValues.reasons ?? reasons,
        // cries: updatedValues.cries ?? cries,
      );
}
