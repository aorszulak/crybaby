import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

enum ThemePreference {
  @JsonValue('system')
  system,
  @JsonValue('light')
  light,
  @JsonValue('dark')
  dark,
}

extension ThemePreferenceExtension on ThemePreference {
  ThemeMode? toThemeMode() {
    switch (this) {
      case ThemePreference.light:
        return ThemeMode.light;
      case ThemePreference.dark:
        return ThemeMode.dark;
      default:
        return null;
    }
  }

  int toIndex() {
    switch (this) {
      case ThemePreference.system:
        return 0;
      case ThemePreference.light:
        return 1;
      case ThemePreference.dark:
        return 2;
    }
  }

  static ThemePreference fromIndex(final int index) {
    switch (index) {
      case 1:
        return ThemePreference.light;
      case 2:
        return ThemePreference.dark;
      default:
        return ThemePreference.system;
    }
  }
}
