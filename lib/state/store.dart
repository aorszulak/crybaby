import 'package:crybaby/state/app_state.dart';
import 'package:crybaby/state/app_state_reducer.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

Future<Store<AppState>> createStore() async {
  final Persistor<AppState> persistor = Persistor<AppState>(
    storage: FlutterStorage(
        location: kIsWeb
            ? FlutterSaveLocation.sharedPreferences
            : FlutterSaveLocation.documentFile),
    serializer: JsonSerializer<AppState>(
      AppState.tryFromJson,
    ),
  );

  final AppState initialState = (await persistor.load()) ?? AppState();

  final Store<AppState> store = Store<AppState>(
    appStateReducer,
    initialState: initialState,
    middleware: <Middleware<AppState>>[persistor.createMiddleware()],
  );

  return store;
}
