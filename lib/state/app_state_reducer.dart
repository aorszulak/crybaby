
import 'package:crybaby/state/app_state.dart';
import 'package:crybaby/state/app_state_actions.dart';

AppState appStateReducer(
  final AppState oldValue,
  final dynamic action,
) {
  if (action is UpdateStateAction) {
    final AppState newValue = oldValue.copyWith(action);
    return newValue;
  } else {
    return oldValue;
  }
}
