import 'package:crybaby/data/cry.dart';
import 'package:crybaby/data/reason.dart';
import 'package:crybaby/state/language_preference.dart';
import 'package:crybaby/state/theme_preference.dart';

class UpdateStateAction {
  const UpdateStateAction({
    this.themePreference,
    this.languagePreference,
    this.reasons,
    this.cries,
  });

  final LanguagePreference? languagePreference;
  final List<Reason>? reasons;
  final List<Cry>? cries;
  final ThemePreference? themePreference;
}
