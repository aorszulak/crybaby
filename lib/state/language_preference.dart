import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

enum LanguagePreference {
  @JsonValue('system')
  system,
  @JsonValue('polish')
  polish,
  @JsonValue('german')
  german,
  @JsonValue('english')
  english,
}

extension LanguagePreferenceExtension on LanguagePreference {
  Locale? toLocale() {
    switch (this) {
      case LanguagePreference.polish:
        return const Locale('pl');
      case LanguagePreference.english:
        return const Locale('en');
      case LanguagePreference.german:
        return const Locale('de');
      default:
        return null;
    }
  }

  static LanguagePreference fromString(final String? value) {
    switch (value) {
      case 'pl':
        return LanguagePreference.polish;
      case 'en':
        return LanguagePreference.english;
      case 'de':
        return LanguagePreference.german;
      default:
        return LanguagePreference.system;
    }
  }
}
