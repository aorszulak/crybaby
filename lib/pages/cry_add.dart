import 'dart:developer';

import 'package:crybaby/components/cry_form.dart';
import 'package:crybaby/data/cry.dart';
import 'package:crybaby/data/cry_form_value.dart';
import 'package:crybaby/data/cry_service.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class CryAddPage extends StatefulWidget {
  const CryAddPage({super.key});

  @override
  State<CryAddPage> createState() => _CryAddPageState();
}

class _CryAddPageState extends State<CryAddPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<CryFormState> _formWidgetKey = GlobalKey<CryFormState>();

  Future<void> _onPressed() async {
    try {
      final bool? valid = _formKey.currentState?.validate();
      if (valid != true) {
        return;
      }
      _formKey.currentState?.save();
      final CryFormValue? cryFormValue = _formWidgetKey.currentState?.cry;
      if (cryFormValue == null) {
        return;
      }
      final Cry cry = Cry(
        cryFormValue.duration.inMinutes,
        cryFormValue.intensity,
        cryFormValue.timestamp,
        cryFormValue.reasons,
      );
      await CryService.add(cry);
      if (!mounted) {
        return;
      }
      context.pop();
    } catch (e) {
      log('e: $e');
    }
  }

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar.large(
            title: Text('Add cry'),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: CryForm(
                key: _formWidgetKey,
                formKey: _formKey,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onPressed,
        child: const Icon(Icons.save),
      ),
    );
  }
}
