import 'package:flutter/material.dart';
import 'package:flutter_adaptive_scaffold/flutter_adaptive_scaffold.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage(
      {required this.navigationShell, required this.children, super.key});

  final StatefulNavigationShell navigationShell;
  final List<Widget> children;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late PageController _pageController;

  @override
  void initState() {
    _pageController = PageController(
      initialPage: widget.navigationShell.currentIndex,
    );
    super.initState();
  }

  @override
  Widget build(final BuildContext context) {
    return AdaptiveScaffold(
      useDrawer: false,
      body: (final _) => PageView(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: widget.children,
      ),
      selectedIndex: widget.navigationShell.currentIndex,
      destinations: <NavigationDestination>[
        NavigationDestination(
          icon: Icon(MdiIcons.homeOutline),
          selectedIcon: Icon(MdiIcons.home),
          label: 'Dashboard',
        ),
        NavigationDestination(
          icon: Icon(MdiIcons.emoticonCryOutline),
          selectedIcon: Icon(MdiIcons.emoticonCry),
          label: 'Cries',
        ),
        NavigationDestination(
          icon: Icon(MdiIcons.emoticonSadOutline),
          selectedIcon: Icon(MdiIcons.emoticonSad),
          label: 'Reasons',
        ),
      ],
      onSelectedIndexChange: (final int newIndex) async {
        await _pageController.animateToPage(
          newIndex,
          duration: const Duration(milliseconds: 200),
          curve: Curves.easeInOut,
        );
        widget.navigationShell.goBranch(newIndex);
      },
    );
  }
}
