import 'package:crybaby/data/reason.dart';
import 'package:crybaby/data/reason_service.dart';
import 'package:crybaby/router/app_router.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class ReasonIndexPage extends StatefulWidget {
  const ReasonIndexPage({super.key});

  @override
  State<ReasonIndexPage> createState() => _ReasonIndexPageState();
}

class _ReasonIndexPageState extends State<ReasonIndexPage> {
  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar.large(
            title: Text('Reasons'),
          ),
          ValueListenableBuilder<Box<Reason>>(
            valueListenable: ReasonService.box.listenable(),
            builder: (final BuildContext context, final Box<Reason> box,
                final Widget? widget) {
              return SliverList.separated(
                itemBuilder: (final BuildContext context, final int index) {
                  final Reason? reason = box.getAt(index);
                  if (reason == null) {
                    return const SizedBox(
                      height: 20,
                    );
                  }
                  return Dismissible(
                    key: reason.key != null
                        ? Key(reason.key?.toString() ?? '')
                        : UniqueKey(),
                    onDismissed: (final DismissDirection direction) async {
                      await reason.delete();
                    },
                    direction: DismissDirection.endToStart,
                    background: Container(
                        color: Theme.of(context).colorScheme.errorContainer),
                    child: ListTile(
                      title: Text(reason.title),
                    ),
                  );
                },
                itemCount: box.length,
                separatorBuilder:
                    (final BuildContext context, final int index) {
                  return const Divider(
                    height: 0,
                  );
                },
              );
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          const ReasonAddRoute().go(context);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
