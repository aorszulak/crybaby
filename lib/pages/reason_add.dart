import 'dart:developer';

import 'package:crybaby/components/reason_form.dart';
import 'package:crybaby/data/reason.dart';
import 'package:crybaby/data/reason_service.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ReasonAddPage extends StatefulWidget {
  const ReasonAddPage({super.key});

  @override
  State<ReasonAddPage> createState() => _ReasonAddPageState();
}

class _ReasonAddPageState extends State<ReasonAddPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ReasonFormState> _formWidgetKey =
      GlobalKey<ReasonFormState>();

  Future<void> _onPressed() async {
    try {
      final bool? valid = _formKey.currentState?.validate();
      if (valid != true) {
        return;
      }
      _formKey.currentState?.save();
      final String? title = _formWidgetKey.currentState?.reason.title;
      if (title == null) {
        return;
      }
      final Reason reason = Reason(title);
      await ReasonService.add(reason);
      if (!mounted) {
        return;
      }
      context.pop();
    } catch (e) {
      log('e: $e');
    }
  }

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar.large(
            title: Text('Add reason'),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: ReasonForm(
                key: _formWidgetKey,
                formKey: _formKey,
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onPressed,
        child: const Icon(Icons.save),
      ),
    );
  }
}
