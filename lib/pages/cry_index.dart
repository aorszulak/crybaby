import 'package:collection/collection.dart';
import 'package:crybaby/data/cry.dart';
import 'package:crybaby/data/cry_service.dart';
import 'package:crybaby/router/app_router.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';

class CryIndexPage extends StatefulWidget {
  const CryIndexPage({super.key});

  @override
  State<CryIndexPage> createState() => _CryIndexPageState();
}

class _CryIndexPageState extends State<CryIndexPage> {
  @override
  Widget build(final BuildContext context) {
    final DateFormat dateFormater = DateFormat('y MMM dd HH:mm');

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar.large(
            title: Text('Cries'),
          ),
          ValueListenableBuilder<Box<Cry>>(
            valueListenable: CryService.box.listenable(),
            builder: (final BuildContext context, final Box<Cry> box,
                final Widget? widget) {
              final List<Cry> values = box.values
                  .sorted((final Cry a, final Cry b) =>
                      b.timestamp.compareTo(a.timestamp))
                  .toList();
              return SliverList.separated(
                itemBuilder: (final BuildContext context, final int index) {
                  final Cry cry = values[index];
                  return Dismissible(
                    key: cry.key != null
                        ? Key(cry.key?.toString() ?? '')
                        : UniqueKey(),
                    onDismissed: (final DismissDirection direction) async {
                      await cry.delete();
                    },
                    direction: DismissDirection.endToStart,
                    background: Container(
                        color: Theme.of(context).colorScheme.errorContainer),
                    child: ListTile(
                      title: Text(dateFormater.format(cry.timestamp)),
                      subtitle: Text(
                          '${cry.duration} minutes, ${cry.intensity}/10 intensity'),
                    ),
                  );
                },
                itemCount: values.length,
                separatorBuilder:
                    (final BuildContext context, final int index) {
                  return const Divider(
                    height: 0,
                  );
                },
              );
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          const CryAddRoute().go(context);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
