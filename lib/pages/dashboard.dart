import 'package:crybaby/components/cards/cries_line.dart';
import 'package:crybaby/data/cry.dart';
import 'package:crybaby/data/cry_service.dart';
import 'package:crybaby/router/app_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_adaptive_scaffold/flutter_adaptive_scaffold.dart';
import 'package:hive_flutter/hive_flutter.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({super.key});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar.large(
            title: Text('Dashboard'),
          ),
          ValueListenableBuilder<Box<Cry>>(
            valueListenable: CryService.box.listenable(),
            builder: (
              final BuildContext context,
              final Box<Cry> box,
              final Widget? widget,
            ) {
              return SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: Breakpoints.large.isActive(context)
                      ? 3
                      : Breakpoints.medium.isActive(context)
                          ? 2
                          : 1,
                  crossAxisSpacing: 16,
                ),
                delegate: SliverChildListDelegate(
                  <Widget>[
                    CriesLineChartCard(
                      box: box,
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await const CryAddRoute().push(context);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
